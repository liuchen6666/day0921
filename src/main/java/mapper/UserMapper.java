package mapper;

import com.javabean.User;

import java.util.ArrayList;

public interface UserMapper {
    ArrayList<User> selectAll();

    //根据条件查询 那个属性有值查询哪个
    //根据user实际传入的参数查询对应数据
//    ArrayList<User> selectByUser(User user);
}
